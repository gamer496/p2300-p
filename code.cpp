#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
using namespace std;
typedef long long ll;


struct nba_team
{
    char *name;
    char conference[30];
    char division[30];
    unsigned short wins;
    unsigned short losses;
    float pct;

    nba_team()
    {
    	wins=0;
    	losses=0;
    }
};

int main()
{
    ios::sync_with_stdio(false);
    nba_team team[35];
    FILE *fp;
    fp=fopen("team.txt","r");
    char line[300];
    int index=0;



    // reading of data from the file and storing it in file
    for(int outer=0;outer<2;outer++)
    {
        fgets(line,sizeof line,fp);
        char conference[30];
        memcpy(conference,&line[0],7);
        conference[7]='\0';
        fgets(line,sizeof line,fp);
        for(int out=0;out<3;out++)
        {
            char division[30];
            fgets(line,sizeof line,fp);
            int fd=0;
            while(line[fd++]!=' ');
            memcpy(division,&line[0],fd-1);
            division[fd-1]='\0';
            for(int ou=0;ou<5;ou++,index++)
            {
            	strcpy(team[index].conference,conference);
            	strcpy(team[index].division,division);
                fgets(line,sizeof line,fp);
                team[index].name=(char *)malloc(strlen(line));
                strcpy(team[index].name,line);
                team[index].name[strlen(line)-1]='\0';
            }
            fgets(line,sizeof line,fp);
        }
    }
    // end of reading data from the file and storing it in structure.


    fp=fopen("score.txt","r");
    while(fgets(line,sizeof line,fp)!=NULL)
    {
    	memmove(line,line+13,strlen(line));
    	if(!(line[0]>=65 && line[0]<=91))
    	{
    		continue;
    	}
    	char team1[40],team2[40];
    	int j=0;
    	for(int i=0;i<strlen(line);i++)
    	{
    		if(line[i]==' ' && line[i+1]==' ')
    		{
    			j=i;
    			break;
    		}
    	}
    	for(int i=0;i<j;i++)
    	{
    		team1[i]=line[i];
    	}
    	team1[j]='\0';
    	while(line[j++]==' ');
    	int k=0;
    	for(int i=j;i<strlen(line);i++)
    	{
    		if(line[i]==' ' && line[i+1]==' ')
    		{
    			k=i;
    			break;
    		}
    	}
    	int f=0;
    	j--;
    	for(int i=j;i<k;i++)
    	{
    		team2[f++]=line[i];
    	}
    	team2[f]='\0';
    	// cout<<team1<<" "<<team2<<endl;
        char score1[5],score2[5];
        int sc1,sc2;
        while(!(line[k]>=48 && line[k]<=57))
        {
            k++;
        }
        int i=0;
        while(line[k]!=',')
        {
            score1[i]=line[k];
            i++;
            k++;
        }
        score1[i]='\0';
        k++;
        i=0;
        while(line[k]!='\n')
        {
            score2[i]=line[k];
            i++;k++;
        }
        score2[i]='\0';
        sscanf(score1,"%d",&sc1);
        sscanf(score2,"%d",&sc2);
        int index1=0,index2=0;
        for(int i=0;i<30;i++)
        {
        	if(strcmp(team[i].name,team1)==0)
        	{
        		index1=i;
        	}
        	if(strcmp(team[i].name,team2)==0)
        	{
        		index2=i;
        	}
        }
        // cout<<index1<<" "<<index2<<endl;
        if(sc1>sc2)
        {
        	team[index1].wins++;
        	team[index2].losses++;
        }
        else
        {
        	team[index1].losses++;
        	team[index2].wins++;
        }
    }

    for(int i=0;i<30;i++)
    {
    	if(team[i].losses==0)
    	{
    		team[i].pct=1;
    	}
    	else
    	{
    		team[i].pct=(team[i].wins*1.0)/((team[i].losses+team[i].wins)*1.0);
    	}
    }

    cout<<"Western Conference"<<"\n"<<endl;
    cout<<"Southwest Division";
    int x=0;
    x=30-strlen("Southwest Division");
    for(int i=0;i<x;i++)
    {
    	cout<<" ";
    }
    cout<<"W  L  PCT"<<endl;
    for(int i=0;i<30;i++)
    {
    	if(strcmp(team[i].conference,"Western")==0 && strcmp(team[i].division,"Southwest")==0)
    	{
    		cout<<team[i].name;
    		int k=strlen(team[i].name);
    		int j=30-k;
    		for(int f=0;f<j;f++)
    		{
    			cout<<" ";
    		}
    		cout<<team[i].wins<<" "<<team[i].losses<<" "<<team[i].pct<<endl;
    	}
    }
    cout<<endl;

    cout<<"Pacific Division";
    x=30-strlen("Pacific Division");
    for(int i=0;i<x;i++)
    {
    	cout<<" ";
    }
    cout<<"W  L  PCT"<<endl;
    for(int i=0;i<30;i++)
    {
    	if(strcmp(team[i].conference,"Western")==0 && strcmp(team[i].division,"Pacific")==0)
    	{
    		cout<<team[i].name;
    		int k=strlen(team[i].name);
    		int j=30-k;
    		for(int f=0;f<j;f++)
    		{
    			cout<<" ";
    		}
    		cout<<team[i].wins<<" "<<team[i].losses<<" "<<team[i].pct<<endl;
    	}
    }
    cout<<endl;

    cout<<"Northwest Division";
    x=30-strlen("Northwest Division");
    for(int i=0;i<x;i++)
    {
    	cout<<" ";
    }
    cout<<"W  L  PCT"<<endl;
    for(int i=0;i<30;i++)
    {
    	if(strcmp(team[i].conference,"Western")==0 && strcmp(team[i].division,"Northwest")==0)
    	{
    		cout<<team[i].name;
    		int k=strlen(team[i].name);
    		int j=30-k;
    		for(int f=0;f<j;f++)
    		{
    			cout<<" ";
    		}
    		cout<<team[i].wins<<" "<<team[i].losses<<" "<<team[i].pct<<endl;
    	}
    }
    cout<<endl;
    cout<<"Eastern Conference"<<"\n"<<endl;
    cout<<"Southeast Division";
    x=30-strlen("Southeast Division");
    for(int i=0;i<x;i++)
    {
    	cout<<" ";
    }
    cout<<"W  L  PCT"<<endl;
    for(int i=0;i<30;i++)
    {
    	if(strcmp(team[i].conference,"Eastern")==0 && strcmp(team[i].division,"Southeast")==0)
    	{
    		cout<<team[i].name;
    		int k=strlen(team[i].name);
    		int j=30-k;
    		for(int f=0;f<j;f++)
    		{
    			cout<<" ";
    		}
    		cout<<team[i].wins<<" "<<team[i].losses<<" "<<team[i].pct<<endl;
    	}
    }
    cout<<endl;
    cout<<"Central Division";
    x=30-strlen("Central Division");
    for(int i=0;i<x;i++)
    {
    	cout<<" ";
    }
    cout<<"W  L  PCT"<<endl;
    for(int i=0;i<30;i++)
    {
    	if(strcmp(team[i].conference,"Eastern")==0 && strcmp(team[i].division,"Central")==0)
    	{
    		cout<<team[i].name;
    		int k=strlen(team[i].name);
    		int j=30-k;
    		for(int f=0;f<j;f++)
    		{
    			cout<<" ";
    		}
    		cout<<team[i].wins<<" "<<team[i].losses<<" "<<team[i].pct<<endl;
    	}
    }
    cout<<endl;
    cout<<"Atlantic Division";
    x=30-strlen("Atlantic Division");
    for(int i=0;i<x;i++)
    {
    	cout<<" ";
    }
    cout<<"W  L  PCT"<<endl;
    for(int i=0;i<30;i++)
    {
    	if(strcmp(team[i].conference,"Eastern")==0 && strcmp(team[i].division,"Atlantic")==0)
    	{
    		cout<<team[i].name;
    		int k=strlen(team[i].name);
    		int j=30-k;
    		for(int f=0;f<j;f++)
    		{
    			cout<<" ";
    		}
    		cout<<team[i].wins<<" "<<team[i].losses<<" "<<team[i].pct<<endl;
    	}
    }
    cout<<endl;


    fclose(fp);
    return 0;
}
